---
title: "Software"
weight: 4
---

To assist translators and publishers of liturgical books and services and the needs of researchers, OCMC provides open-source software.

| Name | Description |
|---------|---------|
| ALWB | Ages Liturgical Workbench is a desktop application designed for use by any jurisdiction, in any country, and for any language.  It is used by Fr. Seraphim Dedes to produce a [Digital Chant Stand](https://dcs.goarch.org) for the Greek Archdiocese of America. ALWB generates a website of liturgical books and services as PDF and HTML in up to two languages side-by-side.  It provides in-context links to audio records and musical scores. The DCS produced by Fr. Seraphim is used widely in the USA and overseas. |
| Doxa | Doxa is a desktop application that can also run in the cloud. It is the latest generation of liturgical software from OCMC.  It is currently under development and combines the functionality of ALWB and OLW. It can generate a [Digital Chant Stand](https://liml.org) with up to three languages side-by-side.|
| [OLW](https://olw.ocmc.org)| The Online Liturgical Workstation is a cloud application that provides a searchable database of the Greek liturgical text and translation, tools for liturgical translators, and the ability to generate PDF files of liturgical books and services in up to three languages side-by-side. Since its deployment, OLW has been used from over 23 countries. |