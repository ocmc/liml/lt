---
title: "Handbooks"
weight: 4
---

A liturgical translator's handbook explains the meaning of a specific liturgical text, potential translation problems, and potential solutions to those problems.  The inspiration for the creation of liturgical translator's handbooks came out of the experience of Michael Colburn, who was a missionary translator with the Wycliffe Bible Translators in Papua New Guinea, and regularly used the Bible translator's handbooks produced by the United Bible Societies.  As part of the research for a Doctor of Theology [dissertation](/pdfs/thesis/auth-colburn-thesis.pdf) at Aristotle University of Thessaloniki, Greece, Michael created five translator's handbooks for hymns of Theophany. He tested them in Alaska, Kenya, and South Korea.  His research demonstrated that use of the handbooks significantly improved the quality of translations.

Available handbooks are listed below. Click blue links to view the files.

| Name | Description |
|---------|---------|
| [ltm-o1-c1-h](/pdfs/thesis/ltm-o1-c1-h.pdf) | Theophany, Ode 1, Canon 1, Heirmos |
| [ltm-o9-c1-t1](/pdfs/thesis/ltm-o9-c1-t1.pdf) | Theophany, Ode 9, Canon 1, Troparion 1 |
| [ltm-o9-c1-t2](/pdfs/thesis/ltm-o9-c1-t2.pdf) | Theophany, Ode 9, Canon 1, Troparion 2 |
| [ltm-o9-c2-t1](/pdfs/thesis/ltm-o9-c2-t1.pdf) | Theophany, Ode 9, Canon 2, Troparion 1 |
| [ltm-o9-c2-t2](/pdfs/thesis/ltm-o9-c2-t2.pdf) | Theophany, Ode 9, Canon 2, Troparion 2 |
