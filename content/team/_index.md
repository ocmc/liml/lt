---
title: "Team"
weight: 4
---

Provision of the OCMC Liturgical Translation Program is the responsibility of an assigned team of OCMC liturgical translation specialists and overseas missionaries, who are responsible to raise their own financial support for this work through donations of individuals and parishes. The team is led by the program manager, Michael Colburn.  The team develops products and provides services for liturgical translation.

Click on the blue links for information about individual team members.

| Member              | Scope | Position | Current Assignment
|----------------------|--------|--------|--------|
| [Jesse Brandow](https://secure.ocmc.org/site/SPageNavigator/Missionaries/Jesse%20and%20Juanita/JesseandJuanitaMissionaryFamily.html) | Greek Metropolis of Mexico | Liturgical Resource Developer, Translation Projects Coordinator | Spanish Digital Chant Stand |
| [Dr. Michael Colburn](https://secure.ocmc.org/site/SPageNavigator/Missionaries/Colburn%20Family/ColburnMissionaryFamily.html) | Global | Program Manager | Managing program and team, developing Doxa |
| [Fr. James Hargrave](https://secure.ocmc.org/site/SPageNavigator/Missionaries/Hargrave%20Family/HargraveMissionaryFamily.html) | Global | Translation Consultant | East Africa: Digital Chant Stand; Translation Projects Coordinator; Translation Training and Consulting |

