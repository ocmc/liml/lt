---
title: "Home"
---

![OCMC Logo](/images/ocmcLogo.png)

# Liturgical Translation Program: 

# Equipping National Translators

The mission of the Orthodox Christian Mission Center [(OCMC)](https://ocmc.org) is to make disciples of all nations by bringing people to Christ and His Church.  OCMC partners with Orthodox Churches around the world, assisting them as they strive to become vibrant, self-supporting, self-governing, and self-propagating.  The three goals of OCMC are 1) _Bringing People to Christ_, 2) _Establishing His Church_, and 3) _Sharing Christ's Love_.  _Establishing the Church_ involves helping to develop the foundations of a local Church so that it can stand on its own, engage in national and local ministries, and create and sustain vibrant  parishes. One of the components of being "established" and "vibrant" is having services, theological literature and music in the local language(s) and culture. 

For this purpose, OCMC has established a team to develop and manage an on-going program for liturgical translation. The purpose of the program is provide resources for national translators, that is, native-speakers carrying out liturgical translation and publication projects in countries served by OCMC. 

*The members of the OCMC team do not create translations.  Translations should be done by native speakers of the language. The team empowers national translators and national translation projects by providing them resources for their work.* 

This site provides information about the OCMC liturgical translation program and the team assigned to carry it out. 

## Vision

Every Orthodox Christian parish in the world will have the opportunity to worship God using the language spoken by its parishioners. 

## Mission

To equip national translators of the Orthodox liturgical texts with the knowledge and tools they need to produce accurate and understandable translations in their native language so that the translations speak to the hearts of those who pray, hear, or read them.

## Goals

The goals of the OCMC Liturgical Translation Program are:

1. Improve the accuracy and understandability of liturgical translations.

2. Reduce the cost and time required to prepare liturgical publications and websites.

3. Add to the body of knowledge of the theory and practice of liturgical translation.

## Strategies

1. Apply the theory and practice of Bible translation to the task of liturgical translation as applicable.

2. Leverage modern technologies while keeping in mind the needs of people living in locations with limited access to electronic devices and the Internet.

3. Give priority to teaching others how to do something rather than doing it for them.

## How We Achieve the Goals

The OCMC Liturgical Translation Program team achieves its goals by developing and providing free products and services to Eastern Orthodox Christian archdioceses and metropolises in countries served by OCMC.  

The current focus of the team is on the development of courses, handbooks, software and a Digital Chant Stand (DCS) for the Metropolis of Mexico.

To learn about the OCMC team developing these products and delivering services, visit the [team page](/team). 