---
title: "Courses"
weight: 4
---

An important means to improve the quality and understandability of liturgical translations is to provide translators and translation evaluators with formal education.  The OCMC translation team is working on web-based courses that can be taken as an independent study or instructor led, either in person or via the web.  The first such course is currently under development: LT101 Translation Introduction.  It is being developed by team member Fr. James Hargrave.